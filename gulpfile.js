'use strict';

var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserSync = require('browser-sync').create();
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

var concat = require('gulp-concat');
var minifyCSS = require('gulp-minify-css');

var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var rimraf = require('rimraf');

var vendors = [
  'three',
  'three/examples/js/Detector'];

gulp.task('vendors', function () {
  return browserify()
    .require(vendors)
    .bundle()
    .pipe(source('vendors.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('./public/js'));
});

gulp.task('fonts', function () {
  return gulp.src('./src/fonts/**')
    .pipe(gulp.dest('./public/fonts'));
});


gulp.task('images', function () {
  return gulp.src('./src/img/**')
    .pipe(imagemin({
      progressive: true,
      use: [pngquant()]
    }))
    .pipe(gulp.dest('./public/img'));
});

gulp.task('html', function () {
  rimraf('./public/*html', function () {
  });
  return gulp.src('./src/*html')
    .pipe(gulp.dest('./public/'));
});

gulp.task('css', function () {
  rimraf('./public/css/style.min.css', function () {
  });
  return gulp.src(['./node_modules/cropper/dist/cropper.min.css','./src/css/**/*.css'])
    .pipe(minifyCSS())
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('./public/css'));
});

gulp.task('js', function () {
  rimraf('./public/js/script.js', function () {
  });
  return browserify('./src/js/main.js')
    .external(vendors)
    .bundle()
    .pipe(source('script.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('./public/js'));
});

gulp.task('sounds', function () {
  rimraf('./public/audio/*', function () {
  });
  return gulp.src(['./src/audio/sound.mp3'])
    .pipe(gulp.dest('./public/audio'));
});

gulp.task('php', function () {
  rimraf('./public/**.php', function () {
  });
  return gulp.src(['./src/index.php', './src/upload.php'])
    .pipe(gulp.dest('./public/'));
});

gulp.task('php-lib', function () {
  rimraf('./public/lib/', function () {
  });
  return gulp.src(['./src/lib/**/*'])
    .pipe(gulp.dest('./public/lib/'));
});

gulp.task('plain-js', function () {
  rimraf('./public/js/ui.js', function () {
  });
  return gulp.src(['./src/js/ui.js', './src/js/share.js', './src/js/game.js', './node_modules/cropper/dist/cropper.min.js'])
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('./public/js'));
});

gulp.task('js-watch', ['js', 'plain-js'], function (done) {
  browserSync.reload();
  done();
});

gulp.task('html-watch', ['html'], function (done) {
  browserSync.reload();
  done();
});

gulp.task('css-watch', ['css'], function (done) {
  browserSync.reload();
  done();
});

gulp.task('sounds-watch', ['sounds'], function (done) {
  browserSync.reload();
  done();
});

gulp.task('php-watch', ['php'], function (done) {
  browserSync.reload();
  done();
});

gulp.task('serve', ['images', 'html', 'css', 'js', 'plain-js', 'fonts', 'sounds', 'php', 'php-lib'], function () {
  browserSync.init({
    server: {
      baseDir: './public/'
    }
  });

  gulp.watch('./src/js/*js', ['js-watch']);

  gulp.watch('./src/**html', ['html-watch'], function () {
    browserSync.reload();
  });

  gulp.watch('./src/css/**/*.css', ['css-watch'], function () {
    browserSync.reload();
  });

  gulp.watch('./src/audio/**/*', ['sounds-watch'], function () {
    browserSync.reload();
  });

  gulp.watch('./src/**.php', ['php-watch'], function () {
    browserSync.reload();
  });

});

gulp.task('default', function () {
  // Default
});
