module.exports = function (location) {
  location = location || new google.maps.LatLng(55.753564, 37.621085); // Moscow, Red Square

  var panorama = new google.maps.StreetViewPanorama(
    document.getElementById('panoram'), {
      linksControl: false,
      panControl: false,
      enableCloseButton: false,
      disableDefaultUI: true,
      scrollwheel: false,
      clickToGo: false
    });

  var sv = new google.maps.StreetViewService();

  sv.getPanorama({location: location, radius: 50}, processSVData);

  function processSVData(data, status) {
    if (status === google.maps.StreetViewStatus.OK) {
      console.log(data.location.pano);

      panorama.setPano(data.location.pano);
      panorama.setPov({
        heading: 270,
        pitch: 0
      });

      panorama.setVisible(true);

      //loadThreejs(panorama);

    } else {
      console.error('Street View data not found for this location.');
    }
  }
};
