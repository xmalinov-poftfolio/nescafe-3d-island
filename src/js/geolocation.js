var streetViewLoader = require('./swloader.js');

module.exports = function () {

  var getGeoButton = document.getElementById('getGeoButton');

  function success(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;

    streetViewLoader(new google.maps.LatLng(lat, lng));
  }

  function error() {
    console.log('Unable to retrieve your location');
  }

  getGeoButton.addEventListener('click', function () {
    if (!navigator.geolocation) {
      console.log('Geolocation is not supported by your browser');
    } else {
      navigator.geolocation.getCurrentPosition(success, error);
    }
  }, false);
};
