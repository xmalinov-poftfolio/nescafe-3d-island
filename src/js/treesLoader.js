module.exports = function (objMaterial, objFile, objName) {
  // var trees = [];
  //
  // var container = new THREE.Object3D();
  //
  // var mtlLoader = new THREE.MTLLoader();
  // //mtlLoader.setBaseUrl('js/models/tree/');
  // //mtlLoader.setPath('js/models/tree/');
  // var url = 'js/models/tree/ter_nescafe_02.mtl';
  // mtlLoader.load(url, function (materials) {
  //
  //   materials.preload();
  //
  //   var objLoader = new THREE.OBJLoader();
  //   objLoader.setMaterials(materials);
  //   //objLoader.setBaseUrl('js/models/tree/');
  //   //objLoader.setPath('js/models/tree/');
  //   objLoader.load('js/models/tree/ter_nescafe_02.obj', function (object) {
  //     object.receiveShadow = false;
  //     object.casctShadow = false;
  //
  //     object.scale.set(4, 4, 4);
  //
  //     for (var i = 0; i < 20; i++) {
  //       container.add(object.clone());
  //       trees.push( container);
  //     }
  //   });
  //
  // });
  //
  //
  // return trees;

  var container = new THREE.Object3D();
  var mtlLoader = new THREE.MTLLoader();
  var objLoader = new THREE.OBJLoader();

  mtlLoader.load(objMaterial, function (materials) {
    materials.preload();

    objLoader.setMaterials(materials);

    objLoader.load(objFile, function (object) {
      object.receiveShadow = true;
      object.casctShadow = true;

      object.scale.set(4, 4, 4);

      object.name = objName;

      object.traverse(function (child) {
        if (child instanceof THREE.Mesh) {
          child.castShadow = true;
        }
      });

      container.add(object);
    });
  });

  return container;
};
