window.onload = function () {
  //sessionStorage.setItem('lat', location.lat());
  var currentDate = new Date();

  try {
    if (sessionStorage.getItem('expires')) {
      //console.info(Date.parse(new Date) - Date.parse(sessionStorage.getItem('expires')));
      if (Date.parse(new Date) - Date.parse(sessionStorage.getItem('expires')) > 300000) {
        //console.info('expired');
        sessionStorage.removeItem('expires');
        sessionStorage.removeItem('lat');
        sessionStorage.removeItem('lng');

        if (localStorage.getItem('end') == true) {
          localStorage.setItem('step', 0);
        }
      }
    } else {
      sessionStorage.setItem('expires', currentDate);
    }
  } catch (e) {
    // there was an error so...
    console.log('Private mode detected');
  }

  var is3Dmode = false;
  var isFirstLoad = true;

  var isMobile = false;
  if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
    isMobile = true;
  }

  if (!isMobile) {
    $('#audiotrack').attr('src', 'audio/sound.mp3');
    var audio = $('#myAudio');
    audio[0].pause();
    audio[0].load();
    audio[0].oncanplaythrough = audio[0].play();
  }

  var THREE = require('three');

  window.THREE = THREE;

  var MTLLoader = require('three/examples/js/loaders/MTLLoader.js');
  var OBJLoader = require('three/examples/js/loaders/OBJLoader.js');

  var Stats = require('three/examples/js/libs/stats.min.js');
  var DatGui = require('three/examples/js/libs/dat.gui.min.js');
  var GSVPano = require('./GSVPano.js');
  //var TweenMax = require('gsap/TweenMax.js');

  var Detector = require('three/examples/js/Detector');

  var treesLoader = require('./treesLoader');

  //var tmpTree = treesLoader('js/models/tree/ter_nescafe_02.mtl', 'js/models/tree/ter_nescafe_02.obj', 'tree1');
  // var trees = [];
  // for (var i = 0; i < 20; i++) {
  //   trees.push(tmpTree.clone());
  // }

  var treesFlat = [];
  var trees3d = [];

  // Textures loading
  var mainPreloader = document.getElementById('main-preloader');
  var preloaderText = document.getElementById('main-preloader-progress');

  var img_to_load = [
    './img/island-texture-placeholder-mobile.png',
    './img/flare.png',
    './img/leaf0.png',
    './img/leaf1.png',
    './img/leaf2.png',
    './img/leaf3.png',
    './img/leaf4.png',
    './img/leaf5.png',
    './img/leaf6.png',
    './img/leaf7.png',
    './img/leaf8.png',
    './img/leaf9.png',
    './img/placeholder.jpg'
  ];
  var loaded_images = 0;

  var preloaderPercents = {percents: 0};

  function updateHandler() {
    preloaderText.innerHTML = preloaderPercents.percents + '%';
  }

  var waitTime = 3;

  for (var i = 0; i < img_to_load.length; i++) {
    var img = document.createElement('img');
    img.src = img_to_load[i];
    img.style.display = 'hidden'; // don't display preloaded images
    img.onload = function () {

      loaded_images++;
      if (loaded_images == img_to_load.length) {
        //console.log('done loading images');


        TweenMax.to(preloaderPercents, waitTime, {
          percents: '100',
          roundProps: 'percents',
          onUpdate: updateHandler,
          ease: Linear.easeNone,
          onComplete: function () {
            TweenMax.delayedCall(0.1, function () {

              TweenMax.to('#container', 1, {
                opacity: 1,
                onComplete: function () {

                }
              });

              console.info('=== mainprel');
              TweenMax.to(['#main-preloader-animation', '#main-preloader-progress'], 0.3, {
                opacity: 0
              });

              TweenMax.to('#main-preloader', 2, {
                opacity: 0,
                onComplete: function () {

                  mainPreloader.style.display = 'none';
                  isFirstLoad = false;
                }
              });

              // Run game mechanic
              window.isSceneLoaded = true;
              window.runGame();
            });
          }
        });

      }
      else {
        preloaderText.innerHTML = (100 * loaded_images / img_to_load.length).toFixed(0) + '%';
        preloaderPercents.percents = (100 * loaded_images / img_to_load.length).toFixed(0);
        //console.log((100 * loaded_images / img_to_load.length) + '%');
      }
    };
    //document.body.appendChild(img);
  }
  // ===

  function initAutocomplete() {
    var input = document.getElementById('geoinput');

    var searchBox = new google.maps.places.SearchBox(input);

    searchBox.addListener('places_changed', function () {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      places.forEach(function (place) {

        loadPanorama(place.geometry.location);

        // GA event
        dataLayer.push({
          event: 'search_event'
        });

      });

    });
  }

  initAutocomplete();

  // Geolocation API
  function successG(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    loadPanorama(new google.maps.LatLng(lat, lng));
  }

  function errorG() {
    console.log('Unable to retrieve your location');
  }

  if (!navigator.geolocation) {
    console.log('Geolocation is not supported by your browser');
  } else {
    navigator.geolocation.getCurrentPosition(successG, errorG);
  }

  /*
   Custom
   */

  var loader,
    map,
    camera,
    scene,
    renderer,
    sphere,
    attributes,
    container,
    mesh, mesh2,
    marker,
    floor;

  window.markers = [];
  window.glowMarkers = [];

  var stats;

  var maxAnisotropy;

  var panoramLinks = {};
  var prevLocation = null;

  var groundPosY = -500;
  var groundBlocks = [];

  var groundHeight = 5120;
  var groundWidth = 2560;
  var groundOffsets = [groundHeight * 2, groundHeight, 0, -groundHeight, -groundHeight * 2];
  var groundOffsetsCenter = [groundHeight, 0, -groundHeight];
  var groundsCurrNums = [1, 2, 3];

  var particles = [];
  var particleImage = new Image();//THREE.ImageUtils.loadTexture( "img/ParticleSmoke.png" );
  particleImage.src = './img/leaf.png';
  var particleSystem;
  var particleCount = 500;
  var particlesGroup = new THREE.Group();

  var container = document.getElementById('container');
  var preloader = document.getElementById('preloader');
  var bar = document.getElementById('bar');

  var texture_placeholder,
    onMouseDownMouseX = 0,
    onMouseDownMouseY = 0,
    onMouseDownLon = 0,
    onMouseDownLat = 0;

  var fov = 75;
  var isUserInteracting = false;
  var phi = 0;
  var theta = 0;
  var lon = 0;
  var lat = 0;

  var mainGroup = new THREE.Group();

  if (true) {
    $('.mobile-placeholder').hide();
    $('.main-content').show();
    init();
    animate();

  } else {
    $('.mobile-placeholder').show();
    $('.main-content').hide();
    $("#myAudio").prop('muted', true);
  }

  function setProgress(progress) {
    bar.style.width = (preloader.clientWidth - 6) * progress / 100 + 'px';
  }

  function showProgress(show) {
    preloader.style.opacity = (show == true) ? 1 : 0;
    preloader.style.display = (show == true) ? 'block' : 'none';
  }

  function geoError(message) {
    showError(message);
  }

  function showError(message) {
    console.log(message);
  }

  function showMessage(message) {
    showError('');
    console.log(message);
  }

  function init() {
    stats = new Stats();
    stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom

    // Align top-left
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.left = '0px';
    stats.domElement.style.top = '0px';

    //document.body.appendChild(stats.dom);

    container = document.getElementById('container');

    var locations = [
      {
        lat: 55.7614675,
        lng: 37.6098927
      }
    ];

    var pos;

    // if (window.location.hash) {
    //   parts = window.location.hash.substr(1).split(',');
    //   pos = {
    //     lat: parts[0],
    //     lng: parts[1]
    //   };
    // } else {
    //   pos = locations[Math.floor(Math.random() * locations.length)];
    // }

    if (sessionStorage.getItem('lat') && sessionStorage.getItem('lng')) {
      pos = {
        lat: sessionStorage.getItem('lat'),
        lng: sessionStorage.getItem('lng')
      };
    } else {
      pos = locations[Math.floor(Math.random() * locations.length)];
    }

    var myLatlng = new google.maps.LatLng(pos.lat, pos.lng);

    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(fov, window.innerWidth / window.innerHeight, 0.1, 20000);
    camera.target = new THREE.Vector3(0, 0, 0);
    scene.add(camera);

    // Main objects group
    mainGroup.position = new THREE.Vector3(0, 0, 0);
    mainGroup.lookAt(camera.position);

    // show axes
    var axes = new THREE.AxisHelper(20);
    //scene.add(axes);

    // Ground textures

    //var tmpTree = treesLoader('js/models/tree/ter_nescafe_02.mtl', 'js/models/tree/ter_nescafe_02.obj', 'tree');
    //var tmpTrees = treesLoader();
    // console.info(tmpTree);

    function generateGroundPlane(textureName, offset, groundNum) {

      var shadowMaterial = new THREE.MeshBasicMaterial({
        color: 0xffffff,
        map: new THREE.TextureLoader().load('./img/' + textureName),
        transparent: true
      });

      shadowMaterial.map.anisotropy = 16;

      var shadowGeo = new THREE.PlaneGeometry(groundWidth, groundHeight, 10, 10);

      var plane = new THREE.Mesh(shadowGeo, shadowMaterial);

      plane.position.y = groundPosY;
      plane.position.x = offset;
      plane.rotation.x = -90 * Math.PI / 180;
      plane.rotation.z = -90 * Math.PI / 180;

      function generateTree(groundNum, treeNum, markNum) {
        var treeTextureWidth = 663;
        var treeTextureHeight = 730;

        var maxLeft = -groundWidth / 2 + treeTextureWidth;
        var maxRight = groundWidth / 2 - treeTextureWidth;
        var minForward = 350;
        var minBackward = -350;
        var maxFar = (groundWidth / 2 + treeTextureWidth);

        var treeCoords = [
          [
            {x: maxLeft + 200, y: minForward + 800},
            {x: maxRight + 60, y: minForward + 800},
            {x: maxLeft + 10, y: minForward - 900},
            {x: maxRight - 200, y: minForward - 900}
          ],
          [
            {x: maxLeft + 200, y: minForward + 400},
            {x: maxRight + 60, y: minForward + 900},
            {x: maxLeft + 10, y: minForward - 900},
            {x: maxRight - 300, y: minForward - 900}
          ],
          [
            {x: maxLeft + 200, y: minForward + 800},
            {x: maxRight + 60, y: minForward + 800},
            {x: maxLeft + 10, y: minForward - 900},
            {x: maxRight - 200, y: minForward - 900}
          ],
          [
            {x: maxLeft + 200, y: minForward + 400},
            {x: maxRight + 60, y: minForward + 800},
            {x: maxLeft - 50, y: minForward - 900},
            {x: maxRight - 300, y: minForward - 800}
          ],
          [
            {x: maxLeft + 200, y: minForward + 800},
            {x: maxRight + 60, y: minForward + 800},
            {x: maxLeft + 10, y: minForward - 900},
            {x: maxRight - 200, y: minForward - 900}
          ]
        ];

        var tree3dCoords = [
          [
            {x: maxLeft + 200, y: minForward + 800},
            {x: maxRight + 60, y: minForward + 800},
            {x: maxLeft + 10, y: minForward - 900},
            {x: maxRight - 200, y: minForward - 900}
          ],
          [
            {x: maxLeft + 200, y: minForward + 400},
            {x: maxRight + 260, y: minForward + 300},
            {x: maxLeft - 300, y: minForward - 900},
            {x: maxRight - 300, y: minForward - 900}
          ],
          [
            {x: maxLeft + 200, y: minForward + 800},
            {x: maxRight + 260, y: minForward + 300},
            {x: maxLeft - 300, y: minForward - 900},
            {x: maxRight - 200, y: minForward - 900}
          ],
          [
            {x: maxLeft + 200, y: minForward + 400},
            {x: maxRight + 260, y: minForward + 300},
            {x: maxLeft - 300, y: minForward - 900},
            {x: maxRight - 300, y: minForward - 800}
          ],
          [
            {x: maxLeft + 200, y: minForward + 800},
            {x: maxRight + 60, y: minForward + 800},
            {x: maxLeft + 10, y: minForward - 900},
            {x: maxRight - 200, y: minForward - 900}
          ]
        ];

        var treeMaterial = new THREE.MeshBasicMaterial({
          color: 0xffffff,
          map: new THREE.TextureLoader().load('./img/tree-texture.png'),
          transparent: true,
          side: THREE.DoubleSide,
          depthWrite: false,
          depthTest: false
        });

        treeMaterial.map.anisotropy = 16;

        var treeGeo = new THREE.PlaneGeometry(treeTextureWidth, treeTextureHeight, 10, 10);

        var tree = new THREE.Mesh(treeGeo, treeMaterial);

        //tree.lookAt(camera.position);

        tree.position.x = treeCoords[groundNum][treeNum].x;
        tree.position.y = treeCoords[groundNum][treeNum].y;
        tree.position.z = -groundPosY;
        tree.rotation.x = 90 * Math.PI / 180;
        tree.rotation.y = 0;

        tree.scale.set(1.2, 1.2, 1.2);

        tree.quaternion.copy(camera.quaternion);

        tree.up = new THREE.Vector3(0, 0, 1);
        //tree.lookAt(new THREE.Vector3(0, 0, 0));

        //tree.rotation.x = 0;
        //tree.rotation.y = 0
        //tree.rotation.z = 0;
        //tree.rotationAutoUpdate();
        //tree.rotation.x  = -90 * Math.PI / 180;
        //tree.rotation.y = -90 * Math.PI / 180;
        //tree.rotation.z = 180 * Math.PI / 180;

        var vector = new THREE.Vector3(0, 0, 1);
        vector.applyEuler(camera.rotation, camera.rotation.order);
        tree.lookAt(new THREE.Vector3(camera.position.x, camera.position.y, tree.position.z));
        //tree.rotation. = 90 * Math.PI / 180;

        tree.matrixAutoUpdate = false;
        tree.updateMatrix();

        //===
        if (is3Dmode) {
          if (groundNum === 1 || groundNum === 2 || groundNum === 3) {
          }
          var treeClone = treesLoader('js/models/tree/ter_nescafe_02.mtl', 'js/models/tree/ter_nescafe_02.obj', 'tree');

          treeClone.position.x = tree3dCoords[groundNum][treeNum].x;
          treeClone.position.y = tree3dCoords[groundNum][treeNum].y;
          treeClone.position.z = 0;
          treeClone.rotation.x = 90 * Math.PI / 180;
          treeClone.rotation.y = 0;

          treeClone.quaternion.copy(camera.quaternion);

          treeClone.up = new THREE.Vector3(0, 0, 1);
          var vector2 = new THREE.Vector3(0, 0, 1);
          vector2.applyEuler(camera.rotation, camera.rotation.order);
          treeClone.lookAt(new THREE.Vector3(camera.position.x, camera.position.y, treeClone.position.z));

          plane.add(treeClone);

          treeClone.matrixAutoUpdate = false;
          treeClone.updateMatrix();

          treeClone.verticesNeedUpdate = false;
          treeClone.normalsNeedUpdate = false;
        }

        //===

        if (markNum === 'putMark1') {
          //console.info(markNum);

          var marker1Material = new THREE.MeshBasicMaterial({
            color: 0xffffff,
            map: new THREE.TextureLoader().load('./img/marker-1.png'),
            transparent: true,
            depthWrite: true,
            depthTest: true
          });

          marker1Material.map.anisotropy = 16;

          var marker1Geo = new THREE.PlaneGeometry(700, 671, 10, 10);

          var marker1 = new THREE.Mesh(marker1Geo, marker1Material);

          if (is3Dmode) {
            if (!isMobile) {
              marker1.position.y = 500;
              marker1.position.x = -20;
            } else {
              marker1.position.y = -70;
              marker1.position.x = 22;
            }
          } else {
            marker1.position.y = -70;
            marker1.position.x = 22;
          }

          marker1.rotation.x = 0;
          marker1.rotation.y = 0;
          marker1.rotation.z = 0;

          marker1.scale.set(0.12, 0.12, 0.12);

          marker1.name = {marker: 'marker1', plane: groundNum};

          marker1.matrixAutoUpdate = false;
          marker1.updateMatrix();

          // Add marker backglow

          var marker1GlowMaterial = new THREE.MeshBasicMaterial({
            color: 0xffffff,
            map: new THREE.TextureLoader().load('./img/backglow.png'),
            transparent: true,
            depthWrite: true,
            depthTest: true,
            blending: THREE.AdditiveBlending
          });

          marker1GlowMaterial.map.anisotropy = 16;

          var marker1GlowGeo = new THREE.PlaneGeometry(900, 900, 10, 10);

          var marker1Glow = new THREE.Mesh(marker1GlowGeo, marker1GlowMaterial);

          if (is3Dmode) {
            if (!isMobile) {
              marker1Glow.position.y = 500;
              marker1Glow.position.x = -20;
            } else {
              marker1Glow.position.y = -70;
              marker1Glow.position.x = 22;
            }
          } else {
            marker1Glow.position.y = -70;
            marker1Glow.position.x = 22;
          }
          marker1Glow.position.z = -10;

          // marker1Glow.rotation.x = 0;
          // marker1Glow.rotation.y = 0;
          // marker1Glow.rotation.z = 0;

          marker1Glow.scale.set(0.12, 0.12, 0.12);

          marker1Glow.name = {marker: 'marker1Glow', plane: groundNum};

          marker1Glow.matrixAutoUpdate = false;
          marker1Glow.updateMatrix();

          tree.add(marker1Glow);

          glowMarkers.push(marker1Glow);

          // ===

          tree.add(marker1);

          if (is3Dmode) {
            treeClone.add(marker1);
          }

          markers.push(marker1);
        }

        if (markNum === 'putMark2') {
          //console.info(markNum);

          var marker2Material = new THREE.MeshBasicMaterial({
            color: 0xffffff,
            map: new THREE.TextureLoader().load('./img/marker-2.png'),
            transparent: true,
            depthWrite: false,
            depthTest: false
          });

          marker2Material.map.anisotropy = 16;

          var marker2Geo = new THREE.PlaneGeometry(604, 700, 10, 10);

          var marker2 = new THREE.Mesh(marker2Geo, marker2Material);

          if (is3Dmode) {
            if (!isMobile) {
              marker2.position.y = 700;
              marker2.position.x = -100;
            } else {
              marker2.position.y = 60;
              marker2.position.x = -100;
            }
          } else {
            marker2.position.y = 190;
            marker2.position.x = 110;
          }

          marker2.position.z = 0;

          marker2.rotation.x = 0;
          marker2.rotation.y = 0;
          marker2.rotation.z = 0;

          marker2.scale.set(0.4, 0.4, 0.4);

          marker2.name = {marker: 'marker2', plane: groundNum};

          marker2.matrixAutoUpdate = false;
          marker2.updateMatrix();

          // Add back glow
          var marker2GlowMaterial = new THREE.MeshBasicMaterial({
            color: 0xffffff,
            map: new THREE.TextureLoader().load('./img/backglow.png'),
            transparent: true,
            depthWrite: true,
            depthTest: true,
            blending: THREE.AdditiveBlending
          });

          marker2GlowMaterial.map.anisotropy = 16;

          var marker2GlowGeo = new THREE.PlaneGeometry(700, 700, 10, 10);

          var marker2Glow = new THREE.Mesh(marker2GlowGeo, marker2GlowMaterial);

          if (is3Dmode) {
            if (!isMobile) {
              marker2Glow.position.y = 700;
              marker2Glow.position.x = -100;
            } else {
              marker2Glow.position.y = 60;
              marker2Glow.position.x = -100;
            }
          } else {
            marker2Glow.position.y = 190;
            marker2Glow.position.x = 110;
          }
          marker2Glow.position.z = -10;

          // marker2Glow.rotation.x = 0;
          // marker2Glow.rotation.y = 0;
          // marker2Glow.rotation.z = 0;

          marker2Glow.scale.set(0.5, 0.5, 0.5);

          marker2Glow.name = {marker: 'marker2Glow', plane: groundNum};

          marker2Glow.matrixAutoUpdate = false;
          marker2Glow.updateMatrix();

          tree.add(marker2Glow);

          tree.add(marker2);

          glowMarkers.push(marker2Glow);
          // ===

          if (is3Dmode) {
            treeClone.add(marker2);
          }

          // tree.geometry.verticesNeedUpdate = false;
          // tree.geometry.normalsNeedUpdate = false;

          markers.push(marker2);
        }

        // if (groundNum === 2) {
        //   return treeClone;
        // } else {
        //   return tree;
        // }
        treesFlat.push({'treeObj': tree, 'planeNum': groundNum});

        if (is3Dmode) {
          trees3d.push({'treeObj': treeClone, 'planeNum': groundNum});
        }

        return tree;
      }

      plane.add(generateTree(groundNum, 3, 'putMark1'));
      plane.add(generateTree(groundNum, 2));
      plane.add(generateTree(groundNum, 1));
      plane.add(generateTree(groundNum, 0, 'putMark2'));

      // Add Marker3
      var marker3Material = new THREE.MeshBasicMaterial({
        color: 0xffffff,
        map: new THREE.TextureLoader().load('./img/marker-3.png'),
        transparent: true,
        depthWrite: false,
        depthTest: false
      });

      marker3Material.map.anisotropy = 16;

      var marker3Geo = new THREE.PlaneGeometry(700, 671, 10, 10);

      var marker3 = new THREE.Mesh(marker3Geo, marker3Material);

      marker3.position.x = 40;
      marker3.position.y = 1600;
      marker3.position.z = 1700;
      marker3.rotation.x = 90 * Math.PI / 180;
      marker3.rotation.y = 0;

      marker3.scale.set(1.2, 1.2, 1.2);

      marker3.quaternion.copy(camera.quaternion);

      marker3.up = new THREE.Vector3(0, 0, 1);

      var vector = new THREE.Vector3(0, 0, 1);
      vector.applyEuler(camera.rotation, camera.rotation.order);
      marker3.lookAt(new THREE.Vector3(camera.position.x, camera.position.y, marker3.position.z));

      marker3.name = {marker: 'marker3', plane: groundNum};

      marker3.matrixAutoUpdate = true;
      marker3.updateMatrix();

      markers.push(marker3);

      // Add back glow

      var marker3GlowMaterial = new THREE.MeshBasicMaterial({
        color: 0xffffff,
        map: new THREE.TextureLoader().load('./img/backglow.png'),
        transparent: true,
        depthWrite: false,
        depthTest: false,
        blending: THREE.AdditiveBlending
      });

      marker3GlowMaterial.map.anisotropy = 16;

      var marker3GlowGeo = new THREE.PlaneGeometry(700, 671, 10, 10);

      var marker3Glow = new THREE.Mesh(marker3GlowGeo, marker3GlowMaterial);

      marker3Glow.position.x = 40;
      marker3Glow.position.y = 1600;
      marker3Glow.position.z = 1700;
      marker3Glow.rotation.x = 90 * Math.PI / 180;
      marker3Glow.rotation.y = 0;

      marker3Glow.scale.set(1, 1, 1);

      marker3Glow.quaternion.copy(camera.quaternion);

      marker3Glow.up = new THREE.Vector3(0, 0, 1);

      var vector = new THREE.Vector3(0, 0, 1);
      vector.applyEuler(camera.rotation, camera.rotation.order);
      marker3Glow.lookAt(new THREE.Vector3(camera.position.x, camera.position.y, marker3Glow.position.z));

      marker3Glow.name = {marker: 'marker3Glow', plane: groundNum};
      marker3.matrixAutoUpdate = true;
      marker3.updateMatrix();

      plane.add(marker3Glow);
      plane.add(marker3);

      glowMarkers.push(marker3Glow);

      // ===

      if (groundNum === 1) {
        plane.name = '1';
      }
      if (groundNum === 2) {
        plane.name = '2';
      }
      if (groundNum === 3) {
        plane.name = '3';
      }

      // plane.matrixAutoUpdate = false;
      // plane.updateMatrix();

      // trees[0].position.x = 1000;
      // trees[0].position.y = groundPosY;
      // trees[0].position.z = 100;
      // trees[0].rotation.y = -90 * Math.PI / 180;
      //
      // plane.add(trees[0]);
      //console.info(trees[0]);

      return plane;
    }

    var groundTextureName = 'island-texture-placeholder-mobile.png';

    if (isMobile) {
      groundBlocks.push(generateGroundPlane(groundTextureName, groundOffsets[0], 4));
      groundBlocks.push(generateGroundPlane(groundTextureName, groundOffsets[4], 0));
      groundBlocks.push(generateGroundPlane(groundTextureName, groundOffsets[1], 3));
      groundBlocks.push(generateGroundPlane(groundTextureName, groundOffsets[2], 2));
      groundBlocks.push(generateGroundPlane(groundTextureName, groundOffsets[3], 1));
    } else {
      groundBlocks.push(generateGroundPlane(groundTextureName, groundOffsets[0], 4));
      groundBlocks.push(generateGroundPlane(groundTextureName, groundOffsets[4], 0));
      groundBlocks.push(generateGroundPlane(groundTextureName, groundOffsets[1], 3));
      groundBlocks.push(generateGroundPlane(groundTextureName, groundOffsets[2], 2));
      groundBlocks.push(generateGroundPlane(groundTextureName, groundOffsets[3], 1));
    }

    for (var i = 0; i < groundBlocks.length; i++) {
      mainGroup.add(groundBlocks[i]);
    }

    // ===

    var flareMaterial = new THREE.MeshBasicMaterial({
      color: 0xffffff,
      map: new THREE.TextureLoader().load('./img/flare.png'),
      transparent: true,
      opacity: 0.7,
      blending: THREE.AdditiveBlending,
      depthWrite: false
    });

    var flare1g = new THREE.PlaneGeometry(800, 800, 100, 100);

    var flare1 = new THREE.Mesh(flare1g, flareMaterial);
    flare1.position.y = 0;
    flare1.position.x = 400;
    flare1.position.z = 150;
    flare1.rotation.y = -90 * Math.PI / 180;
    scene.add(flare1);

    var flare2 = new THREE.Mesh(flare1g, flareMaterial);
    flare2.position.y = 0;
    flare2.position.x = -400;
    flare2.position.z = -150;
    flare2.rotation.y = 90 * Math.PI / 180;
    scene.add(flare2);

    // Back color

    var backColorMesh = new THREE.Mesh(new THREE.SphereGeometry(17000, 10, 10), new THREE.MeshBasicMaterial({
      color: 0xa2848e,
      opacity: 0.1,
      transparent: true,
      blending: THREE.AdditiveBlending
    }));
    backColorMesh.scale.x = -1;
    scene.add(backColorMesh);

    // ===

    mesh = new THREE.Mesh(new THREE.SphereGeometry(18000, 10, 10), new THREE.MeshBasicMaterial({
      map: new THREE.TextureLoader().load('./img/placeholder.jpg')
    }));
    mesh.scale.x = -1;
    //mainGroup.add(mesh);
    scene.add(mesh);

    // floor

    var floor = new THREE.PlaneGeometry(2000, 2000, 10, 10);

    for (var i = 0, l = floor.vertices.length; i < l; i++) {

      var vertex = floor.vertices[i];
      vertex.x += Math.random() * 20 - 10;
      vertex.y += Math.random() * 2;
      vertex.z += Math.random() * 20 - 10;

    }

    for (var i = 0, l = floor.faces.length; i < l; i++) {

      var face = floor.faces[i];
      face.vertexColors[0] = new THREE.Color().setHSL(Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75);
      face.vertexColors[1] = new THREE.Color().setHSL(Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75);
      face.vertexColors[2] = new THREE.Color().setHSL(Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75);

    }

    var material = new THREE.MeshBasicMaterial({vertexColors: THREE.VertexColors});

    mesh2 = new THREE.Mesh(floor, material);
    mesh2.rotation.x = -90 * Math.PI / 180;
    mesh2.position.y = groundPosY;
    //mesh2.position.x = 0;
    //scene.add(mesh2);
    //mainGroup.add(mesh2);

    var ambientLight = new THREE.AmbientLight('#fff');
    ambientLight.intensity = 2;
    scene.add(ambientLight);

    // ===

    //renderer = Detector.webgl ? new THREE.WebGLRenderer({precision: "lowp", devicePixelRatio:1, antialias:false}) : new THREE.CanvasRenderer();
    renderer = Detector.webgl ? new THREE.WebGLRenderer({antialias: true}) : new THREE.CanvasRenderer();

    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.sortObjects = false;

    maxAnisotropy = renderer.getMaxAnisotropy();

    container.appendChild(renderer.domElement);

    container.addEventListener('mousedown', onMouseDown, false);
    container.addEventListener('mousemove', onMouseMove, false);
    container.addEventListener('mouseup', onMouseUp, false);

    container.addEventListener('touchstart', onMouseDown, false);
    container.addEventListener('touchmove', onMouseMove, false);
    container.addEventListener('touchend', onMouseUp, false);

    scene.add(mainGroup);

    // Leaves particles

    for (var p = 0; p < particleCount; p++) {
      var leafNum = Math.floor(Math.random() * (9 - 0 + 1)) + 0;
      //particles = new THREE.Geometry();
      var leafMaterial = new THREE.MeshBasicMaterial({
        color: 0xffffff,
        map: THREE.ImageUtils.loadTexture('./img/leaf' + leafNum + '.png'),
        blending: THREE.NormalBlending,
        transparent: true,
        depthWrite: false,
        side: THREE.DoubleSide
      });

      var leafGeo = new THREE.PlaneGeometry(16, 16, 10, 10);
      var leaf = new THREE.Mesh(leafGeo, leafMaterial);
      var pX = Math.random() * 2000 - 1000,
        pY = Math.random() * 2000 - 1000,
        pZ = Math.random() * 2000 - 1000;

      leaf.position.set(pX, pY, pZ);
      leaf.rotation.set(pX, pY, pZ);

      particles.push(leaf);
      particlesGroup.add(leaf);
      //scene.add(leaf);
    }

    scene.add(particlesGroup);

    window.addEventListener('resize', onWindowResized, false);

    onWindowResized(null);

    loadPanorama(myLatlng);

    console.info('init currnet plane: ' + window.currentPlane);
  }

  function loadPanorama(location) {

    //console.log(location.lat(), location.lng());

    setProgress(0);
    showProgress(true);

    loader = new GSVPano({
      useWebGL: false,
      zoom: 3
    });

    loader.onSizeChange = function () {

    };

    loader.onProgress = function (p) {
      setProgress(p);
    };

    loader.onError = function (message) {
      showError(message);
      showProgress(false);
    };

    loader.onPanoramaLoad = function () {

      //window.location.hash = location.lat() + ',' + location.lng();

      sessionStorage.setItem('lat', location.lat());
      sessionStorage.setItem('lng', location.lng());

      var source = this.canvas[0];

      mesh.material.map = new THREE.Texture(source);
      mesh.material.map.needsUpdate = true;

      var canvas = document.createElement('canvas');
      var s = 2;
      canvas.width = source.width / s;
      canvas.height = source.height / s;
      var ctx = canvas.getContext('2d');
      ctx.drawImage(source, 0, 0, source.width, source.height, 0, 0, canvas.width, canvas.height);

      //showMessage('Street view data ' + this.copyright + '.');

      showProgress(false);

      // GA event
      dataLayer.push({
        event: 'map_loaded'
      })

      panoramLinks = loader.getLinks();
      drawLinks(panoramLinks);

      camera.fov = fov;
      camera.updateProjectionMatrix();

      if (isFirstLoad) {
        var timeout = TweenMax.delayedCall(1, function () {
          TweenMax.to('#container', 1, {
            opacity: 1,
          });
        });
      } else {
        TweenMax.to(preloaderPercents, 1, {
          percents: '100',
          roundProps: 'percents',
          onUpdate: updateHandler,
          ease: Linear.easeNone
        });

        var timeout = TweenMax.delayedCall(1, function () {
          TweenMax.to('#container', 1, {
            opacity: 1,
            onComplete: function () {
              TweenMax.to([
                '#main-preloader-blurback',
                '#main-preloader-animation',
                '#main-preloader-progress'
              ], 0.3, {
                opacity: 0
              });

              TweenMax.to('#main-preloader', 2, {
                opacity: 0,
                onComplete: function () {
                  mainPreloader.style.display = 'none';
                  document.getElementById('main-preloader-blurback').style.display = 'none';
                }
              });
            }
          });
        });
      }
    };

    loader.load(location);

    if (!prevLocation) {
      prevLocation = location;
    }

    show3dtrees();
  }

  function moveSceneGroup(location) {
    console.info('moveSceneGroup call', location);

    if (!prevLocation) {
      // mainGroup.position.x = 0;
      // mainGroup.position.z = 0;
      prevLocation = location;
    } else {
      var dy = (location.lat() - prevLocation.lat()) * 1e6;
      var dx = (location.lng() - prevLocation.lng()) * 1e6;
      var distance = google.maps.geometry.spherical.computeDistanceBetween(location, prevLocation);
      if (dx < 0) {
        //mainGroup.position.x -= distance * 150;
        var tmpOffset = groundOffsetsCenter.shift();
        groundOffsetsCenter.push(tmpOffset);

        var tmpG = groundsCurrNums.shift();
        groundsCurrNums.push(tmpG);
      } else {
        //mainGroup.position.x += distance * 150;
        var tmpOffset = groundOffsetsCenter.pop();
        groundOffsetsCenter.unshift(tmpOffset);

        var tmpG = groundsCurrNums.pop();
        groundsCurrNums.unshift(tmpG);
      }

      groundBlocks[2].position.x = groundOffsetsCenter[0];
      groundBlocks[3].position.x = groundOffsetsCenter[1];
      groundBlocks[4].position.x = groundOffsetsCenter[2];

      //console.info(groundsCurrNums[1]);

      window.currentPlane = groundsCurrNums[1];
      console.info('currplane: ' + window.currentPlane);
      prevLocation = location;

      console.info(groundsCurrNums);
    }
  }

  function show3dtrees() {
    //console.info('show3dtrees plane: ' + window.currentPlane);
    if (is3Dmode) {
      for (var i = 0; i < treesFlat.length; i++) {
        if (treesFlat[i].planeNum == window.currentPlane) {
          treesFlat[i].treeObj.visible = false;
          trees3d[i].treeObj.visible = true;
          //console.info('s3t show: ' + treesFlat[i].planeNum, trees3d[i].planeNum)
        } else {
          treesFlat[i].treeObj.visible = true;
          trees3d[i].treeObj.visible = false;
          //console.info('s3t hide: ' + treesFlat[i].planeNum, trees3d[i].planeNum)
        }
      }
    } else {
      for (var i = 0; i < treesFlat.length; i++) {
        treesFlat[i].treeObj.visible = true;
      }
    }
  }

  function drawLinks(linksObj) {
    for (var i = 0; i < 10; i++) {
      if (scene.getObjectByName('pano-link' + i)) {
        scene.remove(scene.getObjectByName('pano-link' + i));
      }
    }

    var RADIUS = 10;
    var SEGMENTS = 10;
    var RINGS = 10;

    var hDelta = linksObj.tiles.centerHeading;

    var sphereMaterial = new THREE.MeshBasicMaterial({
      color: 0xCC0000
    });

    for (var i = 0; i < linksObj.links.length; i++) {
      var sphere = new THREE.Mesh(new THREE.SphereGeometry(RADIUS, SEGMENTS, RINGS), sphereMaterial);

      var radius = 500;
      // var phi = (90 - 0) * (Math.PI / 180),
      //   theta = (linksObj.links[i].heading - hDelta ) * (Math.PI / 180),
      //   x = -((radius) * Math.sin(phi) * Math.cos(theta)),
      //   z = ((radius) * Math.sin(phi) * Math.sin(theta)),
      //   y = ((radius) * Math.cos(phi));

      var theta = (linksObj.links[i].heading - hDelta) * (Math.PI / 180);
      var x = -(100 * Math.cos(theta));
      var z = (100 * Math.sin(theta));

      //console.log(x, y, z);

      sphere.position.x = x;
      sphere.position.y = 0;
      sphere.position.z = z;

      sphere.geometry.verticesNeedUpdate = true;
      sphere.geometry.normalsNeedUpdate = true;

      // Add name for link object and save panoram hash-link
      //sphere.name = 'pano-link' + i;
      //sphere.panoID = linksObj.links[i].pano;

      //scene.add(sphere);

      // Draw link plane
      var arrowMaterial = new THREE.MeshBasicMaterial({
        color: 0xffffff,
        map: new THREE.TextureLoader().load('./img/arrow.png'),
        transparent: true,
        side: THREE.DoubleSide,
        depthWrite: false,
        depthTest: false
      });

      var arrowGeo = new THREE.PlaneGeometry(153, 86, 10, 10);
      var arrow = new THREE.Mesh(arrowGeo, arrowMaterial);

      arrow.position.x = -280 * Math.cos(theta);
      arrow.position.y = -160;
      arrow.position.z = -100 * Math.sin(theta);
      arrow.rotation.x = 0;
      arrow.rotation.y = 0;
      arrow.rotation.z = 0;

      arrow.scale.set(0.7, 0.7, 0.7);

      arrow.name = 'pano-link' + i;
      arrow.panoID = linksObj.links[i].pano;

      arrow.up = new THREE.Vector3(0, 0, 1);
      var vector = new THREE.Vector3(0, 0, 1);
      vector.applyEuler(camera.rotation, camera.rotation.order);
      arrow.lookAt(new THREE.Vector3(camera.position.x, camera.position.y, arrow.position.z));
      arrow.rotation.z = Math.round(Math.cos(theta)) * 90 * Math.PI / 180;

      scene.add(arrow);
    }
  }

  function onWindowResized(event) {
    renderer.setSize(container.clientWidth, container.clientHeight);

    camera.fov = fov;
    camera.aspect = container.clientWidth / container.clientHeight;

    camera.updateProjectionMatrix();
  }

  function onMouseDown(event) {
    event.preventDefault();

    isUserInteracting = true;

    if (event.touches !== undefined) {
      event.clientX = event.touches[0].clientX;
      event.clientY = event.touches[0].clientY;
    }

    onPointerDownPointerX = event.clientX;
    onPointerDownPointerY = event.clientY;

    onPointerDownLon = lon;
    onPointerDownLat = lat;

    // Click on the scene to detect pano urls
    captureSceneClick();
  }

  function captureSceneClick() {
    var raycaster = new THREE.Raycaster();
    var raycasterMarker = new THREE.Raycaster();

    var mouseVector = new THREE.Vector3();

    mouseVector.x = 2 * (event.clientX / window.innerWidth) - 1;
    mouseVector.y = 1 - 2 * (event.clientY / window.innerHeight);

    raycaster.setFromCamera(mouseVector, camera);
    raycasterMarker.setFromCamera(mouseVector, camera);

    var intersects = raycaster.intersectObjects(scene.children);
    var intersectsMarkes = raycasterMarker.intersectObjects(markers);

    for (var i = 0; i < intersects.length; i++) {
      if (intersects[i].object.name.slice(0, -1) === 'pano-link') {
        isUserInteracting = false;

        // GA event
        dataLayer.push({
          event: 'move_event'
        });

        // var theta = 10;
        // var x = camera.position.x;
        // var z = camera.position.z;
        //
        // var moveX = x * Math.cos(theta) + z * Math.sin(theta);
        // var moveZ = z * Math.cos(theta) - x * Math.sin(theta);
        //
        // TweenMax.to(camera.position, 1, {
        //   x: moveX,
        //   z: moveZ,
        //   onUpdate: function () {
        //     camera.updateProjectionMatrix();
        //     camera.lookAt(camera.target);
        //   }
        // });

        mainPreloader.style.display = 'block';
        document.getElementById('main-preloader-blurback').style.display = 'block';
        preloaderPercents.percents = 0;
        updateHandler();

        TweenMax.to([
          '#main-preloader-blurback',
          '#main-preloader-animation',
          '#main-preloader-progress'
        ], 0.3, {
          opacity: 1
        });

        TweenMax.to('#main-preloader', 2, {
          opacity: 1
        });

        TweenMax.to('#container', 0.1, {opacity: 0});

        (function (i) {
          //console.log(intersects[i].object.panoID);

          var sv = new google.maps.StreetViewService();
          sv.getPanorama({pano: intersects[i].object.panoID}, processSVData);

          function processSVData(data, status) {
            if (status === google.maps.StreetViewStatus.OK) {
              //console.log(data.location.latLng.lat(), data.location.latLng.lng());
              //console.log(data.location.latLng);
              //loader.load(data.location.latLng);

              var timeout = TweenMax.delayedCall(1, function () {
                moveSceneGroup(data.location.latLng);
                loadPanorama(data.location.latLng);
              });


              //var myLatlng = new google.maps.LatLng(pos.lat, pos.lng);
            } else {
              console.error('Street View data not found for this location.');
            }
          }

          //intersects[i].object.material.color.set(Math.random() * 0xffffff); // TODO fix reference properties from objects
        })(i);
      }
    }

    if (intersectsMarkes[0] != undefined) {
      isUserInteracting = false;

      if (intersectsMarkes[0].object.name.marker === 'marker1') {
        // GA event
        dataLayer.push({
          event: 'nescafe_photo'
        });

        console.info('marker1 click', intersectsMarkes[0].object.name.plane);
        window.changeStep(2);
      }

      if (intersectsMarkes[0].object.name.marker === 'marker2') {
        // GA event
        dataLayer.push({
          event: 'nescafe_new_pack'
        });

        console.info('marker2 click', intersectsMarkes[0].object.name.plane);
        window.changeStep(4);
      }

      if (intersectsMarkes[0].object.name.marker === 'marker3') {
        // GA event
        dataLayer.push({
          event: 'nescafe_afisha'
        });

        console.info('marker3 click', intersectsMarkes[0].object.name.plane);
        window.changeStep(7);
      }
    }
  }

  function onMouseMove(event) {
    if (isUserInteracting) {
      if (event.touches !== undefined) {
        event.clientX = event.touches[0].clientX;
        event.clientY = event.touches[0].clientY;
      }

      lon = (event.clientX - onPointerDownPointerX) * 0.1 + onPointerDownLon;
      lat = (event.clientY - onPointerDownPointerY) * 0.1 + onPointerDownLat;
      //console.log(camera.rotation);
    }
  }

  function onMouseUp(event) {
    isUserInteracting = false;
  }

  function animate() {
    //stats.update();

    render();
    animateLeaves();

    requestAnimationFrame(animate);
  }

  var isFirstStepReady = false;

  var marker3counter = 0;

  var t = 0;

  function render() {
    for (var i = 0; i < markers.length; i++) {
      if (markers[i].name.marker === 'marker3' && markers[i].visible) {
        markers[i].position.set(markers[i].position.x, markers[i].position.y, 70 * Math.sin(t * 0.02) + 1700);
        markers[i].__dirtyPosition = true;
        markers[i].geometry.verticesNeedUpdate = true;
      }
    }

    for (var i = 0; i < glowMarkers.length; i++) {
      if (glowMarkers[i].name.marker === 'marker3Glow' && glowMarkers[i].visible) {
        glowMarkers[i].position.set(glowMarkers[i].position.x, glowMarkers[i].position.y, 70 * Math.sin(t * 0.02) + 1750);
      }

      glowMarkers[i].rotation.z = t * 0.0016;
      glowMarkers[i].__dirtyPosition = true;
      glowMarkers[i].geometry.verticesNeedUpdate = true;
      glowMarkers[i].updateMatrix();
    }

    t += 1;

    lat = Math.max(-85, Math.min(85, lat));
    phi = (90 - lat) * Math.PI / 180;
    theta = lon * Math.PI / 180;

    camera.position.x = -100 * Math.sin(phi) * Math.cos(theta);
    camera.position.y = -100 * Math.cos(phi);
    camera.position.z = 100 * Math.sin(phi) * Math.sin(theta);

    camera.lookAt(camera.target);

    renderer.render(scene, camera);

    if (window.currentStep === 1 && !isFirstStepReady) {
      //console.info('first step');
      isFirstStepReady = true;

      //console.info(plane);
    }
  }

  function animateLeaves() {
    var pCount = particleCount;

    while (pCount--) {
      var particle = particles[pCount];
      var signMod = Math.random() < 0.5 ? -1 : 1;

      if (particle.position.y > -500) {
        particle.position.x += Math.sin(Math.random() * particle.position.y / 1000) * 4;
        particle.position.z += Math.sin(Math.random() * particle.position.y / 1000) * 3;
        particle.position.y -= 0.5;
        particle.rotation.x += 0.01;
        particle.rotation.y += 0.03;
        particle.rotation.z += 0.02;
      } else {
        var pX = Math.random() * 2000 - 1000,
          pY = Math.random() * 2000 - 1000,
          pZ = Math.random() * 2000 - 1000;

        particle.position.set(pX, pY, pZ);
      }
    }
  }

};
