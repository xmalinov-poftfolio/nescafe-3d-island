var isSceneLoaded = false;
var currentStep = Number(localStorage.getItem('step')) || 0;
//console.info('currstep: ' + currentStep);
var currentPlane = 2;

var isMobile = false;
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
  || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
  isMobile = true;
}

function runGame() {
  //console.log('Run game...');

  hideMarkers();

  if (currentStep === 0) {
    if (!isMobile) {
      // Show first modal
      setTimeout(function () {
        showModal('modalMoreGifts');
      }, 1000);
    } else {
      changeStep(3);
    }
  } else {
    var s = localStorage.getItem('step');
    changeStep(Number(s));
    //console.info('game load')
  }

}

$('#modalMoreGiftsButton').on('click', function () {
  hideModal('modalMoreGifts');

  if (!isMobile) {
    changeStep(1);
  } else {
    changeStep(3);
  }

});
$('#closeModalMoreGifts').on('click', function () {
  hideModal('modalMoreGifts');

  if (!isMobile) {
    changeStep(1);
  } else {
    changeStep(3);
  }
});

$('#closeModalUpload').on('click', function () {
  hideModal('modalUpload');

  changeStep(3);
});

$('#modalUploadEditorNext').on('click', function () {
  hideModal('modalUploadEditor');

  changeStep(3);
});
$('#closeModalUploadEditor').on('click', function () {
  hideModal('modalUploadEditor');

  changeStep(3);
});

$('#closeModalExpand').on('click', function () {
  hideModal('modalExpand');

  changeStep(5);

  console.info('=== ', !!isAuth);

  if (!!isAuth == true) {
    console.info('need url reset');
    window.location = window.location.pathname;
  }
});
$('#closeModalExpand2').on('click', function () {
  hideModal('modalExpand');

  changeStep(5);

  console.info('=== ', !!isAuth);

  if (!!isAuth == true) {
    console.info('need url reset');
    window.location = window.location.pathname;
  }
});

$('#closeModalBasic').on('click', function () {
  hideModal('modalBasic');

  changeStep(6);
});

$('#modalBasicButton').on('click', function () {
  hideModal('modalBasic');

  changeStep(6);
});

$('#closeModalAfisha').on('click', function () {
  hideModal('modalAfisha');
  hideMarkers();

  localStorage.setItem('step', 8);

  localStorage.setItem('end', true);

  //console.info(isAuth);

  if (isAuth == true) {
    //window.location = window.location.pathname;
  }
});


$('#closeModalFlower').on('click', function () {
  hideModal('modalFlower');
});

$('#modalFlowerButton').on('click', function () {
  hideModal('modalFlower');
});


function showMarker(planeNum, markerName) {
  hideMarkers();

  for (var i = 0; i < markers.length; i++) {
    if (markers[i].name.plane === planeNum && markers[i].name.marker === markerName) {
      markers[i].visible = true;
      glowMarkers[i].visible = true;
    }
  }
}

function hideMarkers() {
  //console.info(markers);
  for (var i = 0; i < markers.length; i++) {
    markers[i].visible = false;
    glowMarkers[i].visible = false;
  }
}

function changeStep(stepNum) {
  if (!isMobile) {
    if (stepNum === 1) {
      currentStep = 1;
      showMarker(currentPlane, 'marker1');
    }

    if (stepNum === 2) {
      currentStep = 2;
      showModal('modalUpload');
    }

    if (stepNum === 3) {
      currentStep = 3;

      showMarker(1, 'marker2');

      setTimeout(function () {
        showModal('modalFlower');
      }, 7000);
    }
  } else {
    if (stepNum === 2) {
      currentStep = 2;
      changeStep(3);
    }

    if (stepNum === 3) {
      currentStep = 3;

      showMarker(1, 'marker2');

      setTimeout(function () {
        showModal('modalFlower');
      }, 500);
    }
  }

  if (stepNum === 4) {
    currentStep = 4;
    showModal('modalExpand');
  }

  if (stepNum === 5) {
    currentStep = 5;
    hideMarkers();

    setTimeout(function () {
      showModal('modalBasic');
    }, 7000);
  }

  if (stepNum === 6) {
    currentStep = 6;
    showMarker(3, 'marker3');
  }

  if (stepNum === 7) {
    currentStep = 7;
    showModal('modalAfisha');
  }


  // Save step number
  localStorage.setItem('step', stepNum);

  //console.info(localStorage.getItem('step'));
}
