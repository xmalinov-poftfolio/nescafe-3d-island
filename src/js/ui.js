var jcrop_api, boundx, boundy;

var isMobile = false;
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
  || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
  isMobile = true;
}


function isIE() {

  var ua = window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');

  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
    return true;
  }
  else {
    return false;
  }
}

if (!isMobile) {
  TweenMax.set('.overlay', {
    perspective: 1000
  });
}

TweenMax.set('.modalWindow', {
  transformStyle: 'preserve-3d'
});
TweenMax.set('.modalWindow', {
  rotationX: -90,
  opacity: 0
});
TweenMax.set(['.modalWindow'], {
  backfaceVisibility: 'hidden'
});

function showModal(modalDivId) {
  var $overlay = $('#overlay');
  var $modalDiv = $('#' + modalDivId);

  if (!isMobile) {
    $overlay.fadeIn(300, function () {
      //$modalDiv.removeClass('rot90').addClass('rot0');
    });
  } else {
    $overlay.css({'display': 'block'});
  }

  TweenMax.to($modalDiv, 0.3, {
    opacity: 1,
    onComplete: function () {
      TweenMax.to($modalDiv, 1.2, {
        rotationX: 0,
        ease: Power3.easeOut
      });
    }
  });
}

function hideModal(modalDivId) {
  var $overlay = $('#overlay');
  var $modalDiv = $('#' + modalDivId);

  if (!isMobile) {
    //$modalDiv.removeClass('rot0').addClass('rot90');
    $overlay.fadeOut(1000);
  } else {
    $overlay.css({'display': 'none'});
  }

  TweenMax.to($modalDiv, 1.2, {
    rotationX: -90,
    ease: Power3.easeOut
  });

  TweenMax.to($modalDiv, 1.1, {
    opacity: 0
  });
}

// -- Modal Basic
$('.pBasic').on('click', function () {
  showModal('modalBasic');
});

// -- Modal Black
$('.pBlack').on('click', function () {
  showModal('modalBlack');
});
$('#closeModalBlack').on('click', function () {
  hideModal('modalBlack');
});

// -- Modal Flower
$('.pFlower').on('click', function () {
  showModal('modalFlower');
});

// -- Modal Share
$('.pShare').on('click', function () {
  showModal('modalShare');
});

$('#closeModalShare').on('click', function () {
  hideModal('modalShare');
});

// -- Modal MoreGifts
$('.pMoreGifts').on('click', function () {
  showModal('modalMoreGifts');
});

// -- Modal Afisha
$('.pAfisha').on('click', function () {
  showModal('modalAfisha');
});

$("#license").change(function () {
  if ($('#license').attr('checked')) {
    license.removeAttribute('checked');

    $('#auth-vk-afisha').css({'opacity': 0.5});
    $('#auth-fb-afisha').css({'opacity': 0.5});
    $('#auth-ok-afisha').css({'opacity': 0.5});

    agreedTextInput.style.backgroundImage = 'url(./img/inputOff.png)';

    disableShareAfisha();
  } else {
    license.setAttribute('checked', 'checked');

    $('#auth-vk-afisha').css({'opacity': 1});
    $('#auth-fb-afisha').css({'opacity': 1});
    $('#auth-ok-afisha').css({'opacity': 1});

    agreedTextInput.style.backgroundImage = 'url(./img/inputOn.png)';

    enableShareAfisha();
  }
});

$('#licenseExpand').change(function () {
  if ($('#licenseExpand').attr('checked')) {
    licenseExpand.removeAttribute('checked');

    $('#auth-vk-expand').css({'opacity': 0.5});
    $('#auth-fb-expand').css({'opacity': 0.5});
    $('#auth-ok-expand').css({'opacity': 0.5});

    document.getElementById('agreedTextInputExpand').style.backgroundImage = 'url(./img/inputOff.png)';

    disableShareExpand();
  } else {
    licenseExpand.setAttribute('checked', 'checked');

    $('#auth-vk-expand').css({'opacity': 1});
    $('#auth-fb-expand').css({'opacity': 1});
    $('#auth-ok-expand').css({'opacity': 1});

    document.getElementById('agreedTextInputExpand').style.backgroundImage = 'url(./img/inputOn.png)';

    enableShareExpand();
  }
});


$('.pExpand').on('click', function () {
  showModal('modalExpand');
});

// -- Modal Upload
$('.pUpload').on('click', function () {
  showModal('modalUpload');
});

// -- Modal UploadEditor
$('#modalUploadButton').on('click', function () {
  if (!isMobile) {
    $("#fileInput").click();
  } else {
    hideModal('modalUpload');
    showModal('modalUploadEditor');
  }
});
$('#closeModalUploadEditor').on('click', function () {
  hideModal('modalUploadEditor');
});


$(":file").change(function (e) {
  if (this.files && this.files[0]) {
    // var reader = new FileReader();
    // reader.onload = imageIsLoaded;
    // reader.readAsDataURL(this.files[0]);

    var file = e.target.files[0],
      imageType = /image.*/;

    if (!file.type.match(imageType))
      return;

    var reader = new FileReader();
    reader.onload = imageIsLoaded;
    reader.readAsDataURL(file);
  }
});

function imageIsLoaded(e) {
  $('#crop-canvas').css({'visibity': 'hidden'});
  $('#crop-canvas').attr('src', e.target.result);

  setTimeout(function () {
    $('#crop-canvas').cropper({
      aspectRatio: 1,
      // preview: '.img-preview',
      crop: function (e) {
        // Output the result data for cropping image.
        console.log(e.x);
        console.log(e.y);
        console.log(e.width);
        console.log(e.height);
        console.log(e.rotate);
        console.log(e.scaleX);
        console.log(e.scaleY);
      },
      viewMode: 1,
      modal: true,
      guides: false,
      dragMode: 'move',
      zoomable: false,
      zoomOnTouch: false,
      zoomOnWheel: false
    });
    $('#crop-canvas').css({'visibity': 'visible'});
  }, 1000);

  hideModal('modalUpload');
  showModal('modalUploadEditor');

  $('#crop-ready').on('click', function () {
    $('.img-preview img')[0].src = $('#crop-canvas').cropper('getCroppedCanvas').toDataURL();

    $('.layout-edit').css({'display': 'none'});
    $('.layout-download').css({'display': 'block'});

    var canvas = $('#uploadCanvas')[0];
    var ctx = canvas.getContext('2d');

    var canvas2 = $('#uploadCanvasHidden')[0];
    var ctx2 = canvas2.getContext('2d');

    var img1 = new Image();

    img1.onload = function () {
      var ct = document.getElementById('measure');
      ct.appendChild(img1);

      var wrh = ct.clientWidth / ct.clientHeight;
      var newWidth;
      var newHeight;
      var tmpH;
      var tmpW;

      if (ct.clientWidth > ct.clientHeight) {
        newHeight = canvas.height;
        newWidth = newHeight * wrh;
        ctx.drawImage(
          img1,
          canvas.width / 2 - newWidth / 2,
          0,
          newWidth,
          newHeight
        );

        tmpH = 600 / wrh;
      } else {
        newWidth = canvas.width;
        newHeight = newWidth / wrh;
        ctx.drawImage(
          img1,
          0,
          canvas.height / 2 - newHeight / 2,
          newWidth,
          newHeight
        );

        tmpW = 400 * wrh;
      }

      ct.removeChild(img1);
    };

    //console.info($('.img-preview img')[0].src);

    // console.log($().cropper('getCroppedCanvas'))

    img1.src = $('.img-preview img')[0].src;

    // Blend filter
    var imgBlend = new Image();

    imgBlend.onload = function () {
      var ct = document.getElementById('measure');
      ct.appendChild(imgBlend);
      var wrh = ct.clientWidth / ct.clientHeight;
      var newWidth = canvas.width;
      var newHeight = newWidth / wrh;
      if (newHeight > canvas.height) {
        newHeight = canvas.height;
        newWidth = newHeight * wrh;
      }
      ct.removeChild(imgBlend);
      ctx.globalCompositeOperation = 'lighten';
      ctx.drawImage(imgBlend, 0, 0, newWidth, newHeight);

      var img2 = new Image();

      img2.onload = function () {
        var ct = document.getElementById('measure');
        ct.appendChild(img2);
        var wrh = ct.clientWidth / ct.clientHeight;
        var newWidth = canvas.width;
        var newHeight = newWidth / wrh;
        if (newHeight > canvas.height) {
          newHeight = canvas.height;
          newWidth = newHeight * wrh;
        }
        ct.removeChild(img2);
        ctx.globalCompositeOperation = 'normal';
        ctx.drawImage(img2, 0, 0, newWidth, newHeight);

        createShareImage();
      };

      img2.src = '../img/uploadMask.png';
    };

    if (isIE()) {
      imgBlend.src = '../img/uploadBlend-ie.png';
    } else {
      imgBlend.src = '../img/uploadBlend.jpg';
    }

    // ===

    function createShareImage() {

      var shareImageWidth = 900;
      var shareImageHeight = 404;

      var imgBack = new Image();
      imgBack.onload = function () {
        ctx2.drawImage(imgBack, 0, 0, shareImageWidth, shareImageHeight);
        ctx2.drawImage(canvas, 40, 45, 336, 336);

        // ===
        var imgLogo = new Image();
        imgLogo.onload = function () {
          ctx2.drawImage(imgLogo, 0, 0, shareImageWidth, shareImageHeight);

          $.post('upload.php', {
            data: canvas2.toDataURL('image/png')
          }).done(function (data) {
            //console.info(data);

            var url = 'https://nescafegold.afisha.ru/upload/' + data + '.html';
            var img = 'https://nescafegold.afisha.ru/upload/' + data + '.png';

            localStorage.setItem('afishaURL', url);
            localStorage.setItem('afishaIMG', img);

            var shareUrl1 = url;
            var shareText = 'NESCAFÉGold Весна пришла #NescafeGold #ВстречайВеснуСУдовольствием #ВесеннийСадNescafeGold';
            var shareImg1 = img;

            var urlVK = 'https://vk.com/share.php?url=' + shareUrl1 + '&title=' + encodeURI(shareText) + '&image=' + shareImg1;
            var urlFB = 'http://www.facebook.com/sharer/sharer.php?u=' + shareUrl1 + '&t=' + shareText;
            var urlOK = 'http://www.ok.ru/dk?st.cmd=addShare&st.s=1&st._surl=' + shareUrl1 + '&st.comments=' + shareText;

            $('#vkAfisha').attr({href: urlVK, target: '_blank'});
            $('#fbAfisha').attr({href: urlFB, target: '_blank'});
            $('#okAfisha').attr({href: urlOK, target: '_blank'});
          });
        };
        imgLogo.src = '../img/nescafe-layer2.png';
      };
      imgBack.src = '../img/nescafe-layer1.jpg';
    }

    if (isIE()) {
      $('#saveImageLink').attr('target', '');
    }
  });
}


$('#saveImageLink').on('click', function (e) {
  if (isIE()) {
    e.preventDefault();
  }
  var canvas = document.getElementById('uploadCanvas');
  if (canvas.msToBlob) { //for IE
    var blob = canvas.msToBlob();
    window.navigator.msSaveBlob(blob, 'myfoto.jpg');
  } else {
    var canvas = $('#uploadCanvas')[0];
    $('#saveImageLink').attr('href', canvas.toDataURL('image/jpeg', 0.8));
    $('#saveImageLink').attr('download', 'myfoto.jpg');
  }
});
