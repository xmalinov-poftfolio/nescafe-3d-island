<?php
require_once 'lib/SocialAuther/autoload.php';

// конфигурация настроек адаптера

// Dev
$adapterConfigs = array(
    'vk' => array(
        'client_id' => '5897823',
        'client_secret' => 'neQ3lIqjA6b4GStYQKBB',
        'redirect_uri' => 'https://nescafe-3d-island.bvdp.ru/?auth=vk'
    ),
    'facebook' => array(
        'client_id' => '811920455628819',
        'client_secret' => '46f0109b6aaa0db19e6f8648b1959f0c',
        'redirect_uri' => 'https://nescafe-3d-island.bvdp.ru/?auth=facebook'
    ),
    'odnoklassniki' => array(
        'client_id' => '1250027520',
        'client_secret' => 'F9C93BB38602CCD02CDF3128',
        'redirect_uri' => 'https://nescafe-3d-island.bvdp.ru/?auth=odnoklassniki',
        'public_key' => 'CBAJOCILEBABABABA'
    )
);

// Prod
//$adapterConfigs = array(
//    'vk' => array(
//        'client_id' => '5923771',
//        'client_secret' => 'fzJ1FvZji2a2KFX4GdkG',
//        'redirect_uri' => 'https://nescafegold.afisha.ru/?auth=vk'
//    ),
//    'facebook' => array(
//        'client_id' => '402081280145977',
//        'client_secret' => '3cdda6a90fbb4bebb6422a3578cbf844',
//        'redirect_uri' => 'https://nescafegold.afisha.ru/?auth=facebook'
//    ),
//    'odnoklassniki' => array(
//        'client_id' => '1250244352',
//        'client_secret' => 'AFC7ABA662C9B4F0AB671770',
//        'redirect_uri' => 'https://nescafegold.afisha.ru/?auth=odnoklassniki',
//        'public_key' => 'CBAIDGILEBABABABA'
//    )
//);

$social_type = '';
$social_id = '';
$social_name = '';
$social_email = '';
$social_profile = '';
$social_sex = '';
$social_birthday = '';
$social_userpic = '';

$authURL_1 = '';
$authURL_2 = '';
$authURL_3 = '';

$isAuth = 'false';

// создание адаптеров
$adapters = array();
$authURLs = array();

foreach ($adapterConfigs as $adapter => $settings) {
    $class = 'SocialAuther\Adapter\\' . ucfirst($adapter);
    $adapters[$adapter] = new $class($settings);
}

if (!isset($_GET['code'])) {
    foreach ($adapters as $title => $adapter) {
        //echo '<p><a href="' . $adapter->getAuthUrl() . '">Аутентификация через ' . ucfirst($title) . '</a></p>';

        array_push($authURLs, $adapter->getAuthUrl());
    }

    $authURL_1 = $authURLs[0];
    $authURL_2 = $authURLs[1];
    $authURL_3 = $authURLs[2];

    $isAuth = 'false';
} else {
    if (isset($_GET['auth']) && array_key_exists($_GET['auth'], $adapters)) {
        $auther = new SocialAuther\SocialAuther($adapters[$_GET['auth']]);
    }

    if ($auther->authenticate()) {
        $isAuth = 'true';

        //echo $_GET['auth'] . "<br>";

        if (!is_null($auther->getSocialId())) {
            $social_id = $auther->getSocialId();
            //echo "Социальный ID пользователя: " . $auther->getSocialId() . '<br />';
        }

        if (!is_null($auther->getName())) {
            $social_name = $auther->getName();
            //echo "Имя пользователя: " . $auther->getName() . '<br />';
        }


        if (!is_null($auther->getEmail())) {
            $social_email = $auther->getEmail();
            //echo "Email пользователя: " . $auther->getEmail() . '<br />';
        }

        if (!is_null($auther->getSocialPage())) {
            $social_profile = $auther->getSocialPage();
            //echo "Ссылка на профиль пользователя: " . $auther->getSocialPage() . '<br />';
        }

        if (!is_null($auther->getSex())) {
            $social_sex = $auther->getSex();
            //echo "Пол пользователя: " . $auther->getSex() . '<br />';
        }

        if (!is_null($auther->getBirthday())) {
            $social_birthday = $auther->getBirthday();
            //echo "День Рождения: " . $auther->getBirthday() . '<br />';
        }

        if (!is_null($auther->getAvatar())) {
            $social_userpic = $auther->getAvatar();
            //echo '<img src="' . $auther->getAvatar() . '" />';
            //echo "<br />";
            //echo "<br />";
            //echo "<br />";
        }

        $social_type = $_GET['auth'];

        //echo '<button id="send-post">Test post</button>';
    }
}
?>

<?php
$indexFile = file_get_contents('index1.html');
echo $indexFile;
?>

<script>
    var isAuth = <?php echo "'" . $isAuth . "'" ?>;

    var socialURLs = {
        'url1': <?php echo "'" . $authURL_1 . "'" ?>,
        'url2': <?php echo "'" . $authURL_2 . "'" ?>,
        'url3': <?php echo "'" . $authURL_3 . "'" ?>
    };

    var socialAuthData = {
        'type': <?php echo "'" . $social_type . "'" ?>,
        'id': <?php echo "'" . $social_id . "'"  ?>,
        'name': <?php echo "'" . $social_name . "'" ?>,
        'email': <?php echo "'" . $social_email . "'" ?>,
        'profile': <?php echo "'" . $social_profile . "'" ?>,
        'sex': <?php echo "'" . $social_sex . "'" ?>,
        'birthday': <?php echo "'" . $social_birthday . "'" ?>,
        'userpic': <?php echo "'" . $social_userpic . "'" ?>
    };

    console.info(socialAuthData);
    console.info(socialURLs);
    console.info(isAuth);

    $('#auth-vk-afisha').attr({href: socialURLs.url1});
    $('#auth-fb-afisha').attr({href: socialURLs.url2});
    $('#auth-ok-afisha').attr({href: socialURLs.url3});

    $('#auth-vk-expand').attr({href: socialURLs.url1});
    $('#auth-fb-expand').attr({href: socialURLs.url2});
    $('#auth-ok-expand').attr({href: socialURLs.url3});

    if (isAuth === 'true') {
        $('.agreedText').each(function (index) {
            $(this).css({'display': 'none'});
        });

        $('.agreedText-expand').each(function (index) {
            $(this).css({'display': 'none'});
        });

        $('.auth-buttons').each(function (index) {
            $(this).css({'display': 'none'});
        });

        $('.share-buttons').each(function (index) {
            $(this).css({'display': 'block'});
        });

        $('#vkAfisha').css({'display': 'none'});
        $('#fbAfisha').css({'display': 'none'});
        $('#okAfisha').css({'display': 'none'});

        $('#vkExpand').css({'display': 'none'});
        $('#fbExpand').css({'display': 'none'});
        $('#okExpand').css({'display': 'none'});

        if (socialAuthData.type === 'vk') {
            $('#vkAfisha').css({'display': 'block'});
            $('#vkExpand').css({'display': 'block'});
        }

        if (socialAuthData.type === 'facebook') {
            $('#fbAfisha').css({'display': 'block'});
            $('#fbExpand').css({'display': 'block'});
        }

        if (socialAuthData.type === 'odnoklassniki') {
            $('#okAfisha').css({'display': 'block'});
            $('#okExpand').css({'display': 'block'});
        }

        $('.share-buttons').each(function (index) {
            $(this).on('click', function () {
                $.post('/admin/api/report.php', JSON.stringify({
                    action: 'update',
                    socnet: socialAuthData.type,
                    name: socialAuthData.name,
                    id: socialAuthData.id,
                    photo: socialAuthData.userpic
                })).done(function (data) {
                    console.log(data);
                    console.info('Данные ушли');
                    console.log('current=== ' + currentStep);

                    if (currentStep === 4) {
                        hideModal('modalExpand');
                        changeStep(5);

                        window.location = window.location.pathname;
                    }

                    if (currentStep === 7) {
                        hideModal('modalAfisha');
                        hideMarkers();
                        localStorage.setItem('step', 8);
                    }
                });
            });
        });
    }
</script>

</body>
</html>
